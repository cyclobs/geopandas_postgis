# geopandas_postgis

postgis extensions for geopandas

geopandas is lacking to_postgis method (see https://github.com/geopandas/geopandas/pull/457 )

add a to_postgis method to GeoDataFrame, with following extensions:

  - gfd.geometry is converted to Geometry (if crs/srid) or Geography if no srid
  - shapely.geometry types supported  
  

Performance: add method='copy' to allowed to_sql method's. (this is the default : use method='default' or method=None to fallback legacy)
  
pandas to_sql postgres 'COPY' instead of 'INSERT' is not implemented.
geopandas_postgis override to_sql def to provide method='copy' ( compatible with if_exist='append', using tempora


```
from sqlalchemy import create_engine
import geopandas as gpd
import geopandas_postgis
geopandas_postgis.add_postgis(gpd)

gdf=gpd.GeoDataFrame(df,geometry='geom')

# dump to postgis
gdf.to_postgis('table_name',create_engine('postgresql://user:pass@host:5432/base_name'))

```