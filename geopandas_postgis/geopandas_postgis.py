from geoalchemy2 import Geography, Geometry, WKTElement, WKBElement
from psycopg2.extensions import adapt, register_adapter, AsIs, QuotedString
import shapely as shp
import shapely.geometry as shpg
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects import postgresql
from sqlalchemy import text, MetaData, inspect  # , Table
from sqlalchemy import select, Table
from sqlalchemy.sql import except_all
import string
import random
import csv
from io import StringIO
import pandas as pd
import geopandas as gpd
import os
import logging


logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.WARNING)
logging.getLogger("sqlalchemy.dialects.postgresql").setLevel(logging.ERROR)

shapely_handled_types = [
    shpg.Polygon,
    shpg.Point,
    shpg.LinearRing,
    shpg.LineString,
    shpg.MultiPoint,
    shpg.MultiLineString,
    shpg.MultiPolygon,
]


# temporary table for pandas
# https://gist.github.com/alecxe/44682f79b18f0c82a99c
class TemporaryPandasTable(pd.io.sql.SQLTable):
    def _execute_create(self):
        # Inserting table into database, add to MetaData object
        self.table = self.table.tometadata(self.pd_sql.meta)

        # allow creation of temporary tables
        if "TEMPORARY" not in self.table._prefixes:
            self.table._prefixes.append("TEMPORARY")

        self.table.create()


def get_srid(crs):
    """
    Extract the srid from a crs dict or proj string. If no srid can be
    extracted, return -1, or  0 if crs is None
     Parameters
    ----------
    crs : A dict or proj string crs. Example: {'init': 'epsg:4326'} or
          '+init=epsg:4326'.
    """
    if crs is None:
        return 0
    srid = 0
    if isinstance(crs, dict):
        if "init" in crs:
            s = crs["init"].split("epsg:")
            if len(s) > 0:
                srid = crs["init"].split("epsg:")[1]
    elif isinstance(crs, str):
        s = crs.split("epsg:")
        if len(s) > 0:
            srid = crs.split("epsg:")[1].split(" ")[0]
    return int(srid)


# Alternative to_sql() *method* for DBs that support COPY FROM
def method_copy(table, conn, keys, data_iter):
    # https://pandas.pydata.org/pandas-docs/version/0.24.2/user_guide/io.html#io-sql-method
    # https://github.com/pandas-dev/pandas/pull/21401
    logger.debug("using method_copy")
    # gets a DBAPI connection that can provide a cursor
    dbapi_conn = conn.connection
    with dbapi_conn.cursor() as cur:
        s_buf = StringIO()
        writer = csv.writer(s_buf)
        writer.writerows(data_iter)
        s_buf.seek(0)

        columns = ", ".join('"{}"'.format(k) for k in keys)
        if table.schema:
            table_name = '"{}"."{}"'.format(table.schema, table.name)
        else:
            table_name = '"%s"' % table.name

        sql = "COPY {} ({}) FROM STDIN WITH CSV".format(table_name, columns)
        # logger.debug('method_copy sql code: %s' % sql)
        cur.copy_expert(sql=sql, file=s_buf)


to_sql_methods = {
    "copy": method_copy,
}


def to_sql(df, *args, **kwargs):
    """
    Improved to_sql to take advantage of PostgreSQL dialect benefits.
    Parameters:
    df : pandas.DataFrame
        The DataFrame to be written to the SQL table.
    *args : tuple
        Positional arguments, where the first argument is the table name (str) and the second is the connection or engine.
    **kwargs : dict
        Keyword arguments for additional options.
    Keyword Arguments:
    method : str, optional
        The method to use for inserting data. Default is 'copy'.
    if_exists : str, optional
        What to do if the table already exists. Default is 'fail'.
    schema : str, optional
        The schema to write to. Default is None.
    temp_method : {'pandas', 'sql'}, optional
        Method to create a temporary table. Default is 'pandas'.
        'sql' : Creates a real table *like* the destination table, and drops it at the end.
        'pandas' : Uses TemporaryPandasTable with the same columns order.
    dtype : dict, optional
        Specifying the data types for columns.
    Returns:
    int
        The number of rows affected by the to_sql operation.
    Raises:
    ValueError
        If there is a column mismatch between the DataFrame and the existing table.
    Notes:
    - If attempting to append to an existing table, a temporary table is created and inserted into the previous table, taking care of on_conflict.
    - Index column names from the DataFrame are used as the primary key.
    - The schema is prepended to the search_path.
    - Logs the updated search_path.
    - If the 'replace' mode is used, all rows in the existing table are deleted before appending new data.
    - If the 'append' mode is used and the DataFrame columns are not in the same order as the existing table, the columns are rearranged.
    - Temporary tables are used for fast updates with the specified method.
    - If a temporary table is used, it is merged with the destination table, handling conflicts appropriately.
    """
    logger.debug("using wrapped to_sql")

    table = args[0]
    con_or_engine = args[1]

    # Check if the connection or engine has a connect method
    if hasattr(con_or_engine, "connect"):
        con = con_or_engine.connect()
    else:
        con = con_or_engine

    # Set default method to 'copy' if not provided
    if "method" not in kwargs:
        logger.debug('setting "copy" as default method for to_sql')
        kwargs["method"] = "copy"

    # Set default if_exists to 'fail' if not provided
    if "if_exists" not in kwargs:
        kwargs["if_exists"] = "fail"

    # Set default schema to None if not provided
    if "schema" not in kwargs:
        kwargs["schema"] = None

    # Set default temp_method to 'pandas' if not provided
    if "temp_method" not in kwargs:
        kwargs["temp_method"] = "pandas"

    # Get the default schema name if schema is None
    if kwargs["schema"] is None:
        kwargs["schema"] = inspect(con).default_schema_name

    # Get current search path
    current_search_path = con.execute("SHOW search_path").fetchone()[0]

    # Check if schema is first in search path, if not set it first
    current_schemas = current_search_path.split(",")
    if not current_schemas or current_schemas[0] != kwargs["schema"]:
        con.execute(f"SET search_path TO {kwargs['schema']},{current_search_path}")

    # Log the updated search_path
    updated_search_path = con.execute("SHOW search_path").fetchone()[0]
    logger.info(f"Updated search_path: {updated_search_path}")

    # Replace method with function if available
    if kwargs["method"] in to_sql_methods:
        kwargs["method"] = to_sql_methods[kwargs["method"]]

    # Get database metadata
    metadata = MetaData(schema=kwargs["schema"])
    metadata.reflect(bind=con)
    schema_table = f"{kwargs['schema']}.{table}"

    # Print if table exists
    if schema_table in metadata.tables:
        logger.info(f"Table {table} exists in schema {kwargs['schema']}")
    else:
        logger.info(f"Table {table} does not exist in schema {kwargs['schema']}")

    tmp_table = None
    new_table = False

    # Handle 'replace' mode
    if kwargs["if_exists"] == "replace":
        if schema_table in metadata.tables:
            # Delete all rows in the table (not drop), then append, to prevent drop cascade
            logger.info("replace mode : deleting all rows in existing table %s" % table)
            con.execute(f"delete from {table}")
        # Force append, because old table is empty. Temporary table not needed
        kwargs["if_exists"] = "append"

    # Handle 'append' mode
    elif (kwargs["if_exists"] == "append") and (schema_table in metadata.tables):
        # Destination table exists. Ensure that DataFrame has the same columns ordering
        table_orm = metadata.tables[schema_table]
        non_pk_cols = [
            c.name
            for c in table_orm.columns
            if c not in list(table_orm.primary_key.columns)
        ]
        logger.debug(f"non_pk_cols: {non_pk_cols}")
        if list(df.keys()) != non_pk_cols:
            logger.debug(
                "df columns are not in the same order as existing table. Rearranging them"
            )
            try:
                # Rearrange DataFrame columns to match the existing table
                df = df.reindex(columns=non_pk_cols, copy=False)
            except Exception as e:
                raise ValueError(
                    "Columns mismatch. need more checks for useful debug msg: %s"
                    % str(e)
                )

        logger.info(
            "Temporary table used for fast update with method %s"
            % kwargs["temp_method"]
        )
        if kwargs["temp_method"] == "sql":
            # create a *fake* temporary table like destination table, but empty
            # we can't create a real temporary table because they are not visible to to_sql_legacy
            # so we will have to drop it
            tmp_table = "_to_sql_tmp_sql__%s_%s" % (
                "".join(random.choice(string.ascii_letters) for i in range(5)),
                table,
            )

            sql_command = text(
                f"""
                DROP TABLE IF EXISTS "{tmp_table}"; 
                CREATE TABLE "{tmp_table}" ( 
                    LIKE "{table}"
                );
            """
            )

            con.execute(sql_command)

            # Check the schema in which the temporary table has been created
            schema_check = con.execute(
                f"""
                SELECT table_schema 
                FROM information_schema.tables 
                WHERE table_name = '{tmp_table}';
            """
            ).fetchone()

            logger.debug(
                f"Temporary table {tmp_table} created in schema: {schema_check[0]}"
            )

            # Reflect the temporary table
            metadata.reflect(bind=con, only=[tmp_table])
            tmp_table_orm = Table(tmp_table, metadata, schema=kwargs["schema"])

            # Check for column mismatch between the temporary table and the destination table
            columns_diff_names = set((c.name for c in tmp_table_orm.columns)) ^ set(
                (c.name for c in table_orm.columns)
            )
            if columns_diff_names:
                logger.debug(
                    "Column mismatch between tmp table %s and dest table %s: %s"
                    % (
                        tmp_table_orm.description,
                        table_orm.description,
                        str(columns_diff_names),
                    )
                )

        elif kwargs["temp_method"] == "pandas":
            # Use a temporary table as a buffer (for upsert copy)
            pandas_engine = pd.io.sql.pandasSQL_builder(con)
            tmp_table = tmp_table = "_to_sql_tmp_pandas__%s_%s" % (
                "".join(random.choice(string.ascii_letters) for i in range(5)),
                table,
            )
            if "dtype" in kwargs:
                dtype = kwargs["dtype"]
            else:
                dtype = None
            tmp_table_pd = TemporaryPandasTable(
                tmp_table, pandas_engine, frame=df, if_exists="replace", dtype=dtype
            )
            tmp_table_orm = tmp_table_pd.table

    else:
        new_table = True

    # Use connection session
    args = list(args)
    if tmp_table is not None:
        args[0] = tmp_table
    args[1] = con
    args = tuple(args)
    del kwargs["temp_method"]

    # Insert into the temporary table
    logger.debug(f"Inserting into the temporary table {tmp_table}")
    ret = df.to_sql_legacy(*args, **kwargs)

    # Define primary key from pandas index if new table
    if new_table and (None not in list(df.index.names)):
        logger.debug("defining primary key from pandas index")
        con.execute(
            """alter table "%s" add constraint "%s_pk" primary key( %s )"""
            % (table, table, ",".join(['"%s"' % q for q in df.index.names]))
        )

    if tmp_table is not None:

        logger.debug("merging tmp table")

        columns_diff_names = set((c.name for c in tmp_table_orm.columns)) ^ set(
            (c.name for c in table_orm.columns)
        )
        if columns_diff_names:
            raise ValueError(
                "Column mismatch between tmp table %s and dest table %s: %s"
                % (
                    tmp_table_orm.description,
                    table_orm.description,
                    str(columns_diff_names),
                )
            )

        # Get non-common rows
        new_rows = except_all(tmp_table_orm.select(), table_orm.select())

        # Insert or update rows
        stmt = postgresql.insert(table_orm).from_select(tmp_table_orm.columns, new_rows)
        if non_pk_cols:
            on_conflict_stmt = stmt.on_conflict_do_update(
                index_elements=table_orm.primary_key.columns,
                set_={k: getattr(stmt.excluded, k) for k in non_pk_cols},
            )
        else:
            logger.debug("only primary key , so do nothing on update")
            on_conflict_stmt = stmt.on_conflict_do_nothing()

        res = con.execute(on_conflict_stmt)
        logger.info("insert/update %s rows" % res.rowcount)

        # Drop the temporary table
        tmp_table_orm.drop(con)  # only useful if a fake table was used

    return ret


def get_all_geometry_names(gdf):
    """return all geoemetry columns names (ie those not only the one defined by gdf.geometry"""
    geom_cols = [col for col in gdf.columns if gdf[col].dtype == "geometry"]
    return geom_cols


def to_postgis(gdf, *args, **kwargs):
    """
    like to_sql from geopandas, with following extensions:

    - gfd.geometry is converted to Geometry (if crs/srid) or Geography if no srid
    - shapely.geometry types supported
    - uses a modified and improved version of Dataframe.to_sql. The improved version is
        implemented in this file and its comment documentation must be read to correctly use its features

    """
    logger.debug("using to_postgis extension")

    table = args[0]
    con_or_engine = args[1]

    if hasattr(con_or_engine, "connect"):
        con = con_or_engine.connect()
    else:
        con = con_or_engine

    if kwargs.get("schema", None) is None:
        kwargs["schema"] = inspect(con).default_schema_name

    # Get current search path
    current_search_path = con.execute("SHOW search_path").fetchone()[0]

    # Check if schema is first in search path, if not set it first
    current_schemas = current_search_path.split(",")
    if not current_schemas or current_schemas[0] != kwargs["schema"]:
        con.execute(f"SET search_path TO {kwargs['schema']},{current_search_path}")

    schema_table = f"{kwargs['schema']}.{table}"

    metadata = MetaData(schema=kwargs["schema"])
    metadata.reflect(bind=con)

    srid = get_srid(gdf.crs)
    # if no srid => Geography
    if srid == 0:
        logger.info("no srid : using geographic table")
        Geo = Geography
    else:
        Geo = Geometry

    # set dtype converter for geometry columun
    if "dtype" not in kwargs:
        kwargs["dtype"] = {}

    if "temp_method" not in kwargs:
        kwargs["temp_method"] = "sql"

    geom_cols = gdf.get_all_geometry_names()
    logger.debug(f"geom_cols: {geom_cols}")
    for geom_col in geom_cols:
        geom_types = (
            gdf[geom_col].apply(lambda x: x.__geo_interface__["type"]).unique().tolist()
        )
        if len(geom_types) != 1 and kwargs["temp_method"] != "sql":
            raise TypeError(
                "mixed types not implemented with temp_method='%s' (use 'sql' instead) : %s"
                % (kwargs["temp_method"], ",".join(geom_types))
            )
        geom_type = geom_types[0].upper()
        logger.debug("%s type : %s" % (geom_col, geom_type))
        kwargs["dtype"][geom_col] = Geo(geom_type, srid=srid)  # , srid=srid)

    with con.begin_nested():

        if "if_exists" in kwargs and schema_table in metadata.tables:
            if kwargs["if_exists"] not in ["fail", "replace"]:
                # table will be modified
                # get actual column srid
                with con.begin_nested() as trans:
                    for geom_col in geom_cols:
                        try:
                            column_srid = int(
                                con.execute(
                                    """SELECT Find_SRID('', '%s', '%s');"""
                                    % (table, geom_col)
                                ).fetchone()[0]
                            )
                        except:
                            logger.info(
                                "no column srid on existing %s.%s assuming 0"
                                % (table, geom_col)
                            )
                            column_srid = 0
                    trans.rollback()

                if column_srid != srid:
                    raise ValueError(
                        "geopandas srid %s doesn't match postgis Find_SRID %s"
                        % (srid, column_srid)
                    )

                if column_srid != 0:
                    # overwrite column srid so we don't need to give a srid for each wkt (we be done at the end)
                    logger.debug("temporary removing SRID")
                    for geom_col in geom_cols:
                        con.execute(
                            """SELECT UpdateGeometrySRID('%s','%s',0);"""
                            % (table, geom_col)
                        )

        args = list(args)
        args[1] = con
        args = tuple(args)
        ret = gdf.to_sql(*args, **kwargs)

        if srid != 0:
            logger.debug("restoring SRID %s" % srid)
            for geom_col in geom_cols:
                con.execute(
                    """SELECT UpdateGeometrySRID('%s','%s',%s);"""
                    % (table, geom_col, srid)
                )

    return ret


def add_postgis(gpd):
    logger.warning("add_postgis is deprecated and not needed")


# psycopg2 adapter for shapely.geometry object
def shapelyAdapter(shapely_geo):
    # need to use wkt representation ( pycopg2 fail with wkb :
    #    sqlalchemy.exc.ProgrammingError: (psycopg2.errors.UndefinedFunction) function st_geogfromtext(bytea) does not exist
    return AsIs(adapt(shapely_geo.wkt))


for Shapely_geo in shapely_handled_types:
    register_adapter(Shapely_geo, shapelyAdapter)

gpd.GeoDataFrame.to_postgis = to_postgis
gpd.GeoDataFrame.to_sql_legacy = gpd.GeoDataFrame.to_sql
gpd.GeoDataFrame.to_sql = to_sql
gpd.GeoDataFrame.get_all_geometry_names = get_all_geometry_names
