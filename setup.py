from setuptools import setup 

setup(name='geopandas_postgis',
      description='postgis extensions for geopandas',
      url='https://gitlab.ifremer.fr/cyclobs/geopandas_postgis.git',
      author = "Olivier Archer",
      author_email = "Olivier.Archer@ifremer.fr",
      license='GPL',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      install_requires=['psycopg2','geoalchemy2','geopandas'],
      packages=['geopandas_postgis'],
)
